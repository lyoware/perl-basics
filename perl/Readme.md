

#Conceptos básicos 

##La terminal

La terminal es un programa que permite navegar entre un sistema de directorios, en un entorno grafico podemos decir que es un equivalente a al explorador de archivos, donde se puede:  crear, eliminar, actualizar o leer/abrir/ejecutar archivos o carpetas(directorios), entre otras opciones que veremos más adelante.

Al abrir una terminal lo primero que encontraremos es lo siguiente:

`ajms@MSI-AJ ~$:`

donde:

`usuario@Dominio ~$:`

El usuario: es el nombre en con el cual está actualmente logeado o en uso la terminal 

El dominio: el dominio puede representar el nombre del equipo, ya sea con un nombre o una dirección IP.

La última parte `~$:` es un apuntador o separador, dependiendo de la terminal este puede cambiar un poco, por default el apuntador nos posiciona en la raíz de la carpeta de usuario.
Podemos comprobar nuestra ubicación en todo momento haciendo uso del comando de la terminal: `pwd` y quedaría de la siguiente manera: `usuario@Dominio ~$:pwd` , obteniendo como salida:
`/home/ajms`.

Es necesario conocer la estructura de archivos o directorios que constituyen el sistema, para esto definiremos el sistema de directorios como una estructura de árbol donde la raíz es denotada por `/`, viéndolo desde la consola quedaría de la siguiente manera: `usuario@Dominio ~$:/`. Para comprobarlo podemos hacer uso del comando `cd`.

El comando `cd` permite movernos entre el sistema de archivos gracias a sus variantes,

**Subir a un directorio/raíz padre (retroceder un directorio) ** 
El comando  `cd ..` ejecutado en consola `usuario@Dominio ~$:cd ..` : permite subir un directorio o a la rama padre de este, recordando que por default la terminal nos  posiciona en la carpeta  de  usuario `/home/ajms`, para llegar a la raíz debemos retroceder a dos raíces o directorios arriba del que nos encontramos, para facilitar la comprensión de este tema podemos ver  `/home/ajms` como un árbol:

`/`  
     `---/home`  
        `------/ajms`  

Si ejecutamos el comando `cd ..` una vez partiendo de nuestra posición actual obtendremos `/home`, para ver nuestra posición `pwd` o bien dependiendo de la terminal esta no la mostrará de la siguiente manera `usuario@Dominio ~/home$:` estaría dependiendo la terminal. Representando en árbol:

`/`  
   `---/home`  

Esto quiere decir que hemos subido a un directorio padre o superior, para llegar al directorio raíz aun nos falta subir un directorio más o una raíz más, si ejecutamos de nuevo el comando `cd ..`, obtendremos `/` usando  `pwd` o  `usuario@Dominio ~/$:` revisando el apuntador de la terminal.

**Bajar a un directorio hijo**

El comando  `cd /Directorio` o `cd Directorio`  ejecutado en consola `usuario@Dominio ~$:cd /Documents` : permite bajar o acceder a un directorio o a la rama hija de donde estemos posicionado. Este comando también puede ser usado para moverte  de un directorio a otro a pesar de que no estén en la mima rama ejemplo:

Suponiendo que estamos en la posición por default `/home/ajms` y no deseamos mover a `/usr/bin`
el comando quedaría de la siguiente manera `usuario@Dominio ~$:cd ~/usr/bin` o `usuario@Dominio ~$:cd /usr/bin` el símbolo `~`  puede usarse o no dependiendo la terminal e indica el origen o la raíz

`/`  
    `---/home`  
        `------/ajms`  
    `---/usr`  
        `------/bin`  

El comando `cd --` nos llevara ala posición por default (`/home/ajms`) sin importar en que directorio estemos  

Para conocer un poco mas sobre  la organización de los directorio y sus funciones podemos consultar la siguiente url: https://help.ubuntu.com/kubuntu/desktopguide/es/directories-file-systems.html

**Notas**: 
La terminal como todo programa  necesita entras para poder proporcionar  una salida.
La terminal cuenta con comandos internos que nos permiten  navegar y manipular archivos o directorios.
La terminal permite ejecutar otros programas  y dependiendo del programa este puede tener sus propios comando internos para realizar  acciones sobre su dominio (domino  se refiere a los archivos o servicios sobre los que se tiene control)

**Listado de archivos y directorios**

Para poder realizar  acciones de desplazamiento o manipulacion de directorios  y archivos es necesario  poder ver  los contenidos de los directorios. Para esto usamos  el comando `ls` y sus varaintes.

##Iniciando con perl
Los archivos perl  contienen la extensión 

`nombre_archivo.pl`

Para ejecutar un archivo es necesario otorgar permisos de escritura y lectura o puede uno definirlo a su gusto:

`chmod a+x archivo.pl` 

Para ejecutar un archivo  se utilizara  el operador: `./` seguido del nombre del archivo `.pl`

Permite ejecutar un binario y pasarle argumentos

`#!interpreter [optional-arg]`

ejemplo

`#!/usr/bin/perl print("Hola mundo")`
##variables

##Funciones

##Imprimir 

Para poder imprimir en pantalla  podemos usar la siguiente función nativa

`print()`

como argumentos puede recibir `''` o `""` las comillas simples no son capaces de interpretar variables dentro de su dominio, en cabio las comillas dobles pueden interpretar variables, ejemplo:
