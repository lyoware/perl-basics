#!/usr/bin/perl -w
#read two files and find where the motifs in the sequences appear

use strict;

my $seqfile = "sequences.txt";
my $motifsfile = "motifs.txt";
my (@motif_lines, @seqlines)= ();
my (@seqids, @seqs)= ();
my (@motifids, @motifs)= ();
my @matches= (); 
my $debug = 1; 

#read the data 
open ( SEQS, "<$seqfile") or die("Couldn't open $seqfile: $!\n");
@seqlines = <SEQS>;
close (SEQS);

open (MOTIFS, "<$motifsfile") or die("Couldn't open $motifsfile: $!\n");
@motif_lines=<MOTIFS>;
close (MOTIFS);

#prepare sequences:
for my $i($#seqlines){
    ($seqids[$i],$seqs[$i])= split(/,/, $seqlines[$i]);
    chomp($seqs[$i]);
}

#prepare motifs
for my $i($#motif_lines){
    ($motifids[$i],$motifs[$i])= split( /,/, $motif_lines[$i]);
    chomp($motifs[$i]);
}

#check every moif against all sequences:
for my $i($#motifs){
    print("Looking for $motifs[$i]\n") if $debug;
    my $len = length($motifs[$i]);
    for my $j  $#seqs){
        print("Checking in $seqs[$j] for $motifs[$i]\n") if $debug;
        while ($seqs[$j]=~m/$motifs[$i]/gi){
            my $posn = pos($seqs[$j])- $len;
            print("Matched starting at $posn\n") if $debug;
            push(@matches, "$motifids[$i] is in $seqids[$j] at $posn\n");
        }
     }
 }

 #No matches?
 if ($#matches == -1){
     die("No match found anywhere\n");
 }

 #show matches
 for my $i ($#matches){
     print("$matches[$i]");
 }